import React from 'react';
import { PageHeaderWrapper } from '@ant-design/pro-layout';

export default (): React.ReactNode => (
  <PageHeaderWrapper>
    <div className="welcome-test">Welcome to the HRC Dashboard</div>
  </PageHeaderWrapper>
);
