import React from 'react';
import { Table, Button, Select, Typography } from 'antd';
import { PageHeaderWrapper } from '@ant-design/pro-layout';

const statusButton = <Button type="primary" size={'small'}>Approve</Button>;
const { Option } = Select;
const { Title } = Typography;

const columns = [
  {
    title: 'Batch',
    dataIndex: 'batch',
  },
  {
    title: 'Number of Claims',
    dataIndex: 'numberOfClaims',
  },
  {
    title: 'Actions',
    dataIndex: 'actions',
  },
];

const data: any = [];
for (let i = 0; i < 46; i++) {
  data.push({
    key: i,
    batch: `Batch ${i}`,
    numberOfClaims: `${i + 1}`,
    actions: statusButton,
  });
}

function handleChange(value: any) {
  console.log(`selected ${value}`);
}

class StatusTable extends React.Component {
  state = {
    selectedRowKeys: [],
    loading: false,
  };

  onSelectChange = (selectedRowKeys: any) => {
    console.log('selectedRowKeys changed: ', selectedRowKeys);
    this.setState({ selectedRowKeys });
  };

  render() {
    const { selectedRowKeys } = this.state;
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange,
    };
    const hasSelected = selectedRowKeys.length > 0;

    return (
      <PageHeaderWrapper>
        <Title level={5}>License Key :</Title>
        <Select defaultValue="item1" style={{ width: 120 }} onChange={handleChange}>
          <Option value="item1">License 1</Option>
          <Option value="item2">License 2</Option>
          <Option value="item3">License 3</Option>
        </Select>
        <div>
          <div style={{ marginBottom: 16 }}>
            <span style={{ marginLeft: 8 }}>
              {hasSelected ? `Selected ${selectedRowKeys.length} items` : ''}
            </span>
          </div>
          <Table rowSelection={rowSelection} columns={columns} dataSource={data} />
        </div>
      </PageHeaderWrapper>
    );
  }
}

export default StatusTable;