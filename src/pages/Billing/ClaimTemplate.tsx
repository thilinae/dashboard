import React from 'react';
import { Modal, Button, Table, Typography, Select, Input } from 'antd';
import { PageHeaderWrapper } from '@ant-design/pro-layout';

const { Option } = Select;
const { Title } = Typography;

class AutoApproval extends React.Component {
  state = {
    createVisible: false,
    editVisible: false,
    confirmLoading: false,
    selectedRowKeys: [],
    loading: false,
  };

  onSelectChange = (selectedRowKeys: any) => {
    console.log('selectedRowKeys changed: ', selectedRowKeys);
    this.setState({ selectedRowKeys });
  };

  showCreateModal = () => {
    this.setState({
      createVisible: true,
    });
  };

  handleCreateOk = () => {
    this.setState({
      confirmLoading: true,
    });
    setTimeout(() => {
      this.setState({
        createVisible: false,
        confirmLoading: false,
      });
    }, 2000);
  };

  handleCreateCancel = () => {
    console.log('Clicked cancel button');
    this.setState({
      createVisible: false,
    });
  };

  showEditModal = () => {
    this.setState({
      editVisible: true,
    });
  };

  handleEditOk = () => {
    this.setState({
      confirmLoading: true,
    });
    setTimeout(() => {
      this.setState({
        editVisible: false,
        confirmLoading: false,
      });
    }, 2000);
  };

  handleEditCancel = () => {
    console.log('Clicked cancel button');
    this.setState({
      editVisible: false,
    });
  };

  handleChange = (value: any) => {
    console.log(`selected ${value}`);
  }

  render() {
    const { createVisible, editVisible, confirmLoading, selectedRowKeys } = this.state;
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange,
    };
    const hasSelected = selectedRowKeys.length > 0;

    // Common modal columns
    const modalTableColumns = [
      {
        title: 'Configuration',
        dataIndex: 'configuration',
      },
      {
        title: 'Value',
        dataIndex: 'value',
      },
      {
        title: 'Action',
        dataIndex: 'action',
      },
    ];

    // Modal buttons and inputs
    const configurationInput = <Input placeholder="Configuration" />;
    const valueInput = <Input placeholder="Value" />;
    const addRowButton = <Button type="primary">Add</Button>;
    const removeRowButton = <Button type="primary">Remove</Button>;

    const templateCreateData = [
      {
        key: '1',
        configuration: configurationInput,
        value: valueInput,
        action: addRowButton,
      },
      {
        key: '2',
        configuration: 'FCLASS',
        value: 'CV',
        action: removeRowButton,
      },
      {
        key: '3',
        configuration: 'PROC',
        value: 'U0003',
        action: removeRowButton,
      },
    ];

    const createTemplateModal = <div>
      <Button type="primary" onClick={this.showCreateModal}>
        Create Template...
      </Button>
      <Modal
        title="Create Template"
        visible={createVisible}
        onOk={this.handleCreateOk}
        confirmLoading={confirmLoading}
        onCancel={this.handleCreateCancel}
      >
        <Title level={5}>License Key :</Title>
        <Select defaultValue="item1" style={{ width: 120 }} onChange={this.handleChange}>
          <Option value="item1">License 1</Option>
          <Option value="item2">License 2</Option>
          <Option value="item3">License 3</Option>
        </Select>
        <div>
          <Input placeholder="Template Name" />
        </div>
        <div>
          <div style={{ marginBottom: 16 }}>
            <span style={{ marginLeft: 8 }}>
              {hasSelected ? `Selected ${selectedRowKeys.length} items` : ''}
            </span>
          </div>
          <Table rowSelection={rowSelection} columns={modalTableColumns} dataSource={templateCreateData} />
        </div>
      </Modal>
    </div>;

    const templateEditData = [
      {
        key: '1',
        configuration: configurationInput,
        value: valueInput,
        action: addRowButton,
      },
      {
        key: '2',
        configuration: 'FCLASS',
        value: 'CV',
        action: removeRowButton,
      },
      {
        key: '3',
        configuration: 'PROC',
        value: 'U0003',
        action: removeRowButton,
      },
    ];

    const editTemplateModal = <div>
      <Button type="primary" onClick={this.showEditModal}>
        Edit...
      </Button>
      <Modal
        title="Edit Template"
        visible={editVisible}
        onOk={this.handleEditOk}
        confirmLoading={confirmLoading}
        onCancel={this.handleEditCancel}
      >
        <Title level={5}>License Key :</Title>
        <Select defaultValue="item1" style={{ width: 120 }} onChange={this.handleChange}>
          <Option value="item1">License 1</Option>
          <Option value="item2">License 2</Option>
          <Option value="item3">License 3</Option>
        </Select>
        <div>
          <Input placeholder="Template Name" />
        </div>
        <div>
          <div style={{ marginBottom: 16 }}>
            <span style={{ marginLeft: 8 }}>
              {hasSelected ? `Selected ${selectedRowKeys.length} items` : ''}
            </span>
          </div>
          <Table rowSelection={rowSelection} columns={modalTableColumns} dataSource={templateEditData} />
        </div>
      </Modal>
    </div>;

    const templateDataColumns = [
      {
        title: 'License Key',
        dataIndex: 'license',
      },
      {
        title: 'Template Name',
        dataIndex: 'templateName',
      },
      {
        title: 'Action',
        dataIndex: 'action',
      },
    ];

    const templateData: any = [];
    for (let i = 0; i < 46; i++) {
      templateData.push({
        key: i,
        license: `license ${i}`,
        templateName: `sample template ${i}`,
        action: editTemplateModal,
      });
    }

    return (
      <PageHeaderWrapper>
        <Title level={5}>License Key :</Title>
        <Select defaultValue="item1" style={{ width: 120 }} onChange={this.handleChange}>
          <Option value="item1">License 1</Option>
          <Option value="item2">License 2</Option>
          <Option value="item3">License 3</Option>
        </Select>
        {createTemplateModal}
        <div>
          <div style={{ marginBottom: 16 }}>
            <span style={{ marginLeft: 8 }}>
              {hasSelected ? `Selected ${selectedRowKeys.length} items` : ''}
            </span>
          </div>
          <Table rowSelection={rowSelection} columns={templateDataColumns} dataSource={templateData} />
        </div>
      </PageHeaderWrapper>
    );
  }
}

export default AutoApproval;