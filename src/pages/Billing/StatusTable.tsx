import React from 'react';
import { Table, Button, Select, Typography } from 'antd';
import { PageHeaderWrapper } from '@ant-design/pro-layout';

const statusButton = <Button type="primary" size={'small'}>Process</Button>;
const { Option } = Select;
const { Title } = Typography;

const columns = [
  {
    title: 'Stage Name',
    dataIndex: 'stagename',
  },
  {
    title: 'In-queue Claims',
    dataIndex: 'claims',
  },
  {
    title: 'Status',
    dataIndex: 'status',
  },
  {
    title: 'Process',
    dataIndex: 'process',
  },
];

const data: any = [];
for (let i = 0; i < 46; i++) {
  data.push({
    key: i,
    stagename: `Stage ${i}`,
    claims: `Sample claim text`,
    status: `Status : ${i}`,
    process: statusButton,
  });
}

function handleChange(value: any) {
  console.log(`selected ${value}`);
}

class StatusTable extends React.Component {
  state = {
    selectedRowKeys: [],
    loading: false,
  };

  onSelectChange = (selectedRowKeys: any) => {
    console.log('selectedRowKeys changed: ', selectedRowKeys);
    this.setState({ selectedRowKeys });
  };

  render() {
    const { selectedRowKeys } = this.state;
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange,
    };
    const hasSelected = selectedRowKeys.length > 0;

    return (
      <PageHeaderWrapper>
        <Title level={5}>License Key :</Title>
        <Select defaultValue="item1" style={{ width: 120 }} onChange={handleChange}>
          <Option value="item1">License 1</Option>
          <Option value="item2">License 2</Option>
          <Option value="item3">License 3</Option>
        </Select>
        <div>
          <div style={{ marginBottom: 16 }}>
            <span style={{ marginLeft: 8 }}>
              {hasSelected ? `Selected ${selectedRowKeys.length} items` : ''}
            </span>
          </div>
          <Table rowSelection={rowSelection} columns={columns} dataSource={data} />
        </div>
      </PageHeaderWrapper>
    );
  }
}

export default StatusTable;