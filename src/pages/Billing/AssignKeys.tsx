import React from 'react';
import { Table, Button, Select, Typography } from 'antd';
import { PageHeaderWrapper } from '@ant-design/pro-layout';

const processButton = <Button type="primary" size={'small'}>Remove</Button>;
const { Option } = Select;
const { Title } = Typography;

const columns = [
  {
    title: 'License Key',
    dataIndex: 'licensekey',
  },
  {
    title: 'User',
    dataIndex: 'user',
  },
  {
    title: 'Process',
    dataIndex: 'process',
  }
];

const data: any = [];
for (let i = 0; i < 46; i++) {
  data.push({
    key: i,
    licensekey: `Stage ${i}`,
    user: `Sample claim text`,
    process: processButton,
  });
}

function handleChange(value: any) {
  console.log(`selected ${value}`);
}

class StatusTable extends React.Component {
  state = {
    selectedRowKeys: [],
    loading: false,
  };

  onSelectChange = (selectedRowKeys: any) => {
    console.log('selectedRowKeys changed: ', selectedRowKeys);
    this.setState({ selectedRowKeys });
  };

  render() {
    const { selectedRowKeys } = this.state;
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange,
    };
    const hasSelected = selectedRowKeys.length > 0;

    return (
      <PageHeaderWrapper>
        <Title level={5}>License Key :</Title>
        <Select defaultValue="item1" style={{ width: 120 }} onChange={handleChange}>
          <Option value="item1">License 1</Option>
          <Option value="item2">License 2</Option>
          <Option value="item3">License 3</Option>
        </Select>
        <Title level={5}>User :</Title>
        <Select defaultValue="user1" style={{ width: 120 }} onChange={handleChange}>
          <Option value="user1">User 1</Option>
          <Option value="user1">User 2</Option>
          <Option value="user1">User 3</Option>
        </Select>
        <div>
          <Button type="primary" size={'large'}>Assign</Button>
        </div>
        <div>
          <div style={{ marginBottom: 16 }}>
            <span style={{ marginLeft: 8 }}>
              {hasSelected ? `Selected ${selectedRowKeys.length} items` : ''}
            </span>
          </div>
          <Table rowSelection={rowSelection} columns={columns} dataSource={data} />
        </div>
      </PageHeaderWrapper>
    );
  }
}

export default StatusTable;