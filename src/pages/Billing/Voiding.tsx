import React from 'react';
import { Table, Button } from 'antd';
import { PageHeaderWrapper } from '@ant-design/pro-layout';

const actionButton = <Button type="primary" size={'small'}>Void</Button>;

const columns = [
  {
    title: 'File Name',
    dataIndex: 'filename',
  },
  {
    title: 'License Key',
    dataIndex: 'license',
  },
  {
    title: 'Number of Claims to Void',
    dataIndex: 'numberToVoid',
  },
  {
    title: 'Voided Claims',
    dataIndex: 'voidedClaims',
  },
  {
    title: 'Status',
    dataIndex: 'status',
  },
  {
    title: 'Action',
    dataIndex: 'action',
  },
];

const data: any = [];
for (let i = 0; i < 46; i++) {
  data.push({
    key: i,
    filename: `sample file ${i}`,
    license: `license ${i}`,
    numberToVoid: `${i}`,
    voidedClaims: `${i}`,
    status: `sample`,
    action: actionButton,
  });
}

class StatusTable extends React.Component {
  state = {
    selectedRowKeys: [],
    loading: false,
  };

  onSelectChange = (selectedRowKeys: any) => {
    console.log('selectedRowKeys changed: ', selectedRowKeys);
    this.setState({ selectedRowKeys });
  };

  render() {
    const { selectedRowKeys } = this.state;
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange,
    };
    const hasSelected = selectedRowKeys.length > 0;

    return (
      <PageHeaderWrapper>
        <div>
          <div style={{ marginBottom: 16 }}>
            <span style={{ marginLeft: 8 }}>
              {hasSelected ? `Selected ${selectedRowKeys.length} items` : ''}
            </span>
          </div>
          <Table rowSelection={rowSelection} columns={columns} dataSource={data} />
        </div>
      </PageHeaderWrapper>
    );
  }
}

export default StatusTable;