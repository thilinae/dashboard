import React from 'react';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import { Upload, Select, Typography, Button, message } from 'antd';
import { InboxOutlined } from '@ant-design/icons';

const { Dragger } = Upload;
const { Option } = Select;
const { Title } = Typography;

const props = {
  name: 'file',
  multiple: true,
  action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
  onChange(info: any) {
    const { status } = info.file;
    if (status !== 'uploading') {
      console.log(info.file, info.fileList);
    }
    if (status === 'done') {
      message.success(`${info.file.name} file uploaded successfully.`);
    } else if (status === 'error') {
      message.error(`${info.file.name} file upload failed.`);
    }
  },
};

function handleChange(value: any) {
  console.log(`selected ${value}`);
}

export default (): React.ReactNode => (
  <PageHeaderWrapper>
    <Dragger {...props}>
      <p className="ant-upload-drag-icon">
        <InboxOutlined />
      </p>
      <p className="ant-upload-text">Click or drag file to this area to upload</p>
      {/* <p className="ant-upload-hint">
        Support for a single or bulk upload. Strictly prohibit from uploading company data or other
        band files
    </p> */}
    </Dragger>
    <Title level={5}>Office Key :</Title>
    <Select defaultValue="item1" style={{ width: 120 }} onChange={handleChange}>
      <Option value="item1">item1</Option>
      <Option value="item2">item2</Option>
      <Option value="item3">item3</Option>
    </Select>
    <Title level={5}>Templates:</Title>
    <Select defaultValue="item1" style={{ width: 120 }} onChange={handleChange}>
      <Option value="item1">item1</Option>
      <Option value="item2">item2</Option>
      <Option value="item3">item3</Option>
    </Select>
    <div>
      <Button type="primary" size={'large'}>Upload</Button>
    </div>
  </PageHeaderWrapper>
);
