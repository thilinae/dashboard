import React from 'react';
import { Table, Select, Typography } from 'antd';
import { PageHeaderWrapper } from '@ant-design/pro-layout';

const { Option } = Select;
const { Title } = Typography;

const processedStageColumns = [
  {
    title: 'Processed Stage',
    dataIndex: 'processedStage',
  },
  {
    title: 'Patient Created',
    dataIndex: 'patientCreated',
  },
  {
    title: 'Insurance Created',
    dataIndex: 'insuranceCreated',
  },
];

const processedStageData: any = [];
for (let i = 0; i < 46; i++) {
  processedStageData.push({
    key: i,
    processedStage: `Stage ${i}`,
    patientCreated: `${i + 1}`,
    insuranceCreated: `${i + 1}`,
  });
}

const inProgressColumns = [
  {
    title: 'Stage',
    dataIndex: 'stage',
  },
  {
    title: 'Number of Claims',
    dataIndex: 'numberOfClaims',
  },
];

const inProgressData: any = [];
for (let i = 0; i < 46; i++) {
  inProgressData.push({
    key: i,
    stage: `Stage ${i}`,
    numberOfClaims: `${i + 1}`,
  });
}

function handleChange(value: any) {
  console.log(`selected ${value}`);
}

class StatusTable extends React.Component {
  state = {
    selectedRowKeys: [],
    loading: false,
  };

  onSelectChange = (selectedRowKeys: any) => {
    console.log('selectedRowKeys changed: ', selectedRowKeys);
    this.setState({ selectedRowKeys });
  };

  render() {
    const { selectedRowKeys } = this.state;
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange,
    };
    const hasSelected = selectedRowKeys.length > 0;

    return (
      <PageHeaderWrapper>
        <Title level={5}>License Key :</Title>
        <Select defaultValue="item1" style={{ width: 120 }} onChange={handleChange}>
          <Option value="item1">License 1</Option>
          <Option value="item2">License 2</Option>
          <Option value="item3">License 3</Option>
        </Select>
        <Title level={5}>Processed Stage Status</Title>
        <div>
          <div style={{ marginBottom: 16 }}>
            <span style={{ marginLeft: 8 }}>
              {hasSelected ? `Selected ${selectedRowKeys.length} items` : ''}
            </span>
          </div>
          <Table rowSelection={rowSelection} columns={processedStageColumns} dataSource={processedStageData} />
        </div>
        <Title level={5}>In-Progress Status</Title>
        <div>
          <div style={{ marginBottom: 16 }}>
            <span style={{ marginLeft: 8 }}>
              {hasSelected ? `Selected ${selectedRowKeys.length} items` : ''}
            </span>
          </div>
          <Table rowSelection={rowSelection} columns={inProgressColumns} dataSource={inProgressData} />
        </div>
      </PageHeaderWrapper>
    );
  }
}

export default StatusTable;