import React from 'react';
import { Table } from 'antd';
import { PageHeaderWrapper } from '@ant-design/pro-layout';

const { Column, ColumnGroup } = Table;

const data = [
  {
    key: '1',
    licenseKey: 123123,
    patientJobs: 10,
    patientClaims: 20,
    demographicJobs: 10,
    demographicClaims: 20,
    insuranceJobs: 10,
    insuranceClaims: 20,
    eligibilityJobs: 10,
    eligibilityClaims: 20,
    cptJobs: 10,
    cptClaims: 20,
    visitJobs: 10,
    visitClaims: 20
  },
  {
    key: '2',
    licenseKey: 456456,
    patientJobs: 10,
    patientClaims: 20,
    demographicJobs: 10,
    demographicClaims: 20,
    insuranceJobs: 10,
    insuranceClaims: 20,
    eligibilityJobs: 10,
    eligibilityClaims: 20,
    cptJobs: 10,
    cptClaims: 20,
    visitJobs: 10,
    visitClaims: 20
  },
];

class StatusMonitoring extends React.Component {
  render() {
    return (
      <PageHeaderWrapper>
        <Table dataSource={data}>
          <Column title="License Key" dataIndex="licenseKey" key="licenseKey" />
          <ColumnGroup title="Patient Creation">
            <Column title="Jobs" dataIndex="patientJobs" key="patientJobs" />
            <Column title="Claims" dataIndex="patientClaims" key="patientClaims" />
          </ColumnGroup>
          <ColumnGroup title="Demographic Extraction">
            <Column title="Jobs" dataIndex="demographicJobs" key="demographicJobs" />
            <Column title="Claims" dataIndex="demographicClaims" key="demographicClaims" />
          </ColumnGroup>
          <ColumnGroup title="Insurance Creation">
            <Column title="Jobs" dataIndex="insuranceJobs" key="insuranceJobs" />
            <Column title="Claims" dataIndex="insuranceClaims" key="insuranceClaims" />
          </ColumnGroup>
          <ColumnGroup title="Eligibility Trigger">
            <Column title="Jobs" dataIndex="eligibilityJobs" key="eligibilityJobs" />
            <Column title="Claims" dataIndex="eligibilityClaims" key="eligibilityClaims" />
          </ColumnGroup>
          <ColumnGroup title="CPT">
            <Column title="Jobs" dataIndex="cptJobs" key="cptJobs" />
            <Column title="Claims" dataIndex="cptClaims" key="cptClaims" />
          </ColumnGroup>
          <ColumnGroup title="Visit">
            <Column title="Jobs" dataIndex="visitJobs" key="visitJobs" />
            <Column title="Claims" dataIndex="visitClaims" key="visitClaims" />
          </ColumnGroup>
        </Table>
      </PageHeaderWrapper>
    )
  };
}

export default StatusMonitoring;