import React from 'react';
import { Table, Select, Typography } from 'antd';
import { PageHeaderWrapper } from '@ant-design/pro-layout';

const { Option } = Select;
const { Title } = Typography;
const templateConfiguration =
  <Select defaultValue="config1" style={{ width: 120 }} onChange={handleChange}>
    <Option value="config1">Config 1</Option>
    <Option value="config2">Config 2</Option>
    <Option value="config3">Config 3</Option>
  </Select>;

const columns = [
  {
    title: 'Template Configuration',
    dataIndex: 'tempConfig',
  },
  {
    title: 'Value',
    dataIndex: 'value',
  },
];

const data: any = [];
for (let i = 0; i < 46; i++) {
  data.push({
    key: i,
    tempConfig: templateConfiguration,
    value: `sample value ${i}`,
  });
}

function handleChange(value: any) {
  console.log(`selected ${value}`);
}

class StatusTable extends React.Component {
  state = {
    selectedRowKeys: [],
    loading: false,
  };

  onSelectChange = (selectedRowKeys: any) => {
    console.log('selectedRowKeys changed: ', selectedRowKeys);
    this.setState({ selectedRowKeys });
  };

  render() {
    const { selectedRowKeys } = this.state;
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange,
    };
    const hasSelected = selectedRowKeys.length > 0;

    return (
      <PageHeaderWrapper>
        <Title level={5}>License Key :</Title>
        <Select defaultValue="item1" style={{ width: 120 }} onChange={handleChange}>
          <Option value="item1">License 1</Option>
          <Option value="item2">License 2</Option>
          <Option value="item3">License 3</Option>
        </Select>
        <div>
          <div style={{ marginBottom: 16 }}>
            <span style={{ marginLeft: 8 }}>
              {hasSelected ? `Selected ${selectedRowKeys.length} items` : ''}
            </span>
          </div>
          <Table rowSelection={rowSelection} columns={columns} dataSource={data} />
        </div>
      </PageHeaderWrapper>
    );
  }
}

export default StatusTable;