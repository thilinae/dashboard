import React from 'react';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import { Upload, Select, Typography, Button, message } from 'antd';
import { InboxOutlined } from '@ant-design/icons';

const { Dragger } = Upload;
const { Option } = Select;
const { Title } = Typography;

const props = {
  name: 'file',
  multiple: true,
  action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
  onChange(info: any) {
    const { status } = info.file;
    if (status !== 'uploading') {
      console.log(info.file, info.fileList);
    }
    if (status === 'done') {
      message.success(`${info.file.name} file uploaded successfully.`);
    } else if (status === 'error') {
      message.error(`${info.file.name} file upload failed.`);
    }
  },
};

function handleChange(value: any) {
  console.log(`selected ${value}`);
}

export default (): React.ReactNode => (
  <PageHeaderWrapper>
    <Dragger {...props}>
      <p className="ant-upload-drag-icon">
        <InboxOutlined />
      </p>
      <p className="ant-upload-text">Click or drag file to this area to upload</p>
    </Dragger>
    <div>
      <Button type="primary" size={'large'}>Verify</Button>
    </div>
    <Title level={5}>Error :</Title>
    <div>Error 1</div>
    <div>Error 2</div>
    <div>Error 3</div>
  </PageHeaderWrapper>
);
