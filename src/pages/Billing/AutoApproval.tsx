import React from 'react';
import { Modal, Button, Table } from 'antd';
import { PageHeaderWrapper } from '@ant-design/pro-layout';

class AutoApproval extends React.Component {
  state = {
    ModalText: 'Content of the modal',
    visible: false,
    confirmLoading: false,
    selectedRowKeys: [],
    loading: false,
  };

  onSelectChange = (selectedRowKeys: any) => {
    console.log('selectedRowKeys changed: ', selectedRowKeys);
    this.setState({ selectedRowKeys });
  };

  showModal = () => {
    this.setState({
      visible: true,
    });
  };

  handleOk = () => {
    this.setState({
      ModalText: 'Clicked OK button',
      confirmLoading: true,
    });
    setTimeout(() => {
      this.setState({
        visible: false,
        confirmLoading: false,
      });
    }, 2000);
  };

  handleCancel = () => {
    console.log('Clicked cancel button');
    this.setState({
      visible: false,
    });
  };

  render() {
    const { visible, confirmLoading, ModalText, selectedRowKeys } = this.state;
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange,
    };
    const hasSelected = selectedRowKeys.length > 0;

    const approveModal = <div>
      <Button type="primary" onClick={this.showModal}>
        Approve...
        </Button>
      <Modal
        title="Approve"
        visible={visible}
        onOk={this.handleOk}
        confirmLoading={confirmLoading}
        onCancel={this.handleCancel}
      >
        <p>{ModalText}</p>
      </Modal>
    </div>;

    const columns = [
      {
        title: 'File Name',
        dataIndex: 'filename',
      },
      {
        title: 'License Key',
        dataIndex: 'license',
      },
      {
        title: 'Number of Claims to Void',
        dataIndex: 'numberToVoid',
      },
      {
        title: 'Voided Claims',
        dataIndex: 'voidedClaims',
      },
      {
        title: 'Status',
        dataIndex: 'status',
      },
      {
        title: 'Action',
        dataIndex: 'action',
      },
    ];

    const data: any = [];
    for (let i = 0; i < 46; i++) {
      data.push({
        key: i,
        filename: `sample file ${i}`,
        license: `license ${i}`,
        numberToVoid: `${i}`,
        voidedClaims: `${i}`,
        status: `sample`,
        action: approveModal,
      });
    }

    return (
      <PageHeaderWrapper>
        <div>
          <div style={{ marginBottom: 16 }}>
            <span style={{ marginLeft: 8 }}>
              {hasSelected ? `Selected ${selectedRowKeys.length} items` : ''}
            </span>
          </div>
          <Table rowSelection={rowSelection} columns={columns} dataSource={data} />
        </div>
      </PageHeaderWrapper>
    );
  }
}

export default AutoApproval;