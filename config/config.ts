// https://umijs.org/config/
import { defineConfig } from 'umi';
import defaultSettings from './defaultSettings';
import proxy from './proxy';

const { REACT_APP_ENV } = process.env;

export default defineConfig({
  hash: true,
  antd: {},
  dva: {
    hmr: true,
  },
  locale: {
    // default zh-CN
    default: 'en-US',
    antd: true,
    // default true, when it is true, will use `navigator.language` overwrite default
    baseNavigator: true,
  },
  dynamicImport: {
    loading: '@/components/PageLoading/index',
  },
  targets: {
    ie: 11,
  },
  // umi routes: https://umijs.org/docs/routing
  routes: [
    {
      path: '/',
      redirect: '/login',
    },
    {
      path: '/login',
      component: '../layouts/UserLayout',
      routes: [
        {
          name: 'login',
          path: '/login',
          component: './user/login',
        },
      ],
    },
    {
      path: '/dashboard',
      component: '../layouts/BasicLayout',
      // authority: ['admin', 'user'],
      routes: [
        {
          path: '/dashboard',
          redirect: '/dashboard/welcome',
        },
        {
          path: '/dashboard/welcome',
          name: 'welcome',
          icon: 'table',
          component: './Welcome',
        },
        {
          path: '/dashboard/billing',
          name: 'Billing',
          icon: 'table',
          routes: [
            {
              path: '/dashboard/billing/manage-processing',
              name: 'ManageProcessing',
              component: './Billing/ManageProcessing',
            },
            {
              path: '/dashboard/billing/upload-claims',
              name: 'Upload Claims',
              component: './Billing/UploadClaims',
            },
            {
              path: '/dashboard/billing/status-table',
              name: 'StatusTable',
              component: './Billing/StatusTable',
            },
            {
              path: '/dashboard/billing/approve-claims',
              name: 'ApproveClaims',
              component: './Billing/ApproveClaims',
            },
            {
              path: '/dashboard/billing/assign-keys',
              name: 'AssignKeys',
              component: './Billing/AssignKeys',
            },
            {
              path: '/dashboard/billing/clone-template',
              name: 'CloneTemplate',
              component: './Billing/CloneTemplate',
            },
            {
              path: '/dashboard/billing/create-template',
              name: 'CreateTemplate',
              component: './Billing/CreateTemplate',
            },
            {
              path: '/dashboard/billing/csv-verification',
              name: 'CSVVerification',
              component: './Billing/CSVVerification',
            },
            {
              path: '/dashboard/billing/filewise-processing',
              name: 'FilewiseProcessing',
              component: './Billing/FilewiseProcessing',
            },
            {
              path: '/dashboard/billing/voiding',
              name: 'Voiding',
              component: './Billing/Voiding',
            },
            {
              path: '/dashboard/billing/auto-approval',
              name: 'AutoApproval',
              component: './Billing/AutoApproval',
            },
            {
              path: '/dashboard/billing/claim-process-status',
              name: 'ClaimProcessStatus',
              component: './Billing/ClaimProcessStatus',
            },
            {
              path: '/dashboard/billing/status-monitoring',
              name: 'StatusMonitoring',
              component: './Billing/StatusMonitoring',
            },
            {
              path: '/dashboard/billing/claim-template',
              name: 'ClaimTemplate',
              component: './Billing/ClaimTemplate',
            },
          ],
        },
        {
          path: '/dashboard/posting',
          name: 'Posting',
          icon: 'table',
          routes: [
            {
              path: '/dashboard/posting/posting',
              name: 'Posting',
              component: './Posting/Posting',
            }
          ],
        },
        {
          path: '/dashboard/denial-management',
          name: 'Denial Management',
          icon: 'table',
          routes: [
            {
              path: '/dashboard/denial-management/denial-management',
              name: 'Denial Management',
              component: './DenialManagement/DenialManagement',
            }
          ],
        },
        {
          path: '/dashboard/accounts',
          name: 'Accounts',
          icon: 'table',
          routes: [
            {
              path: '/dashboard/accounts/accounts',
              name: 'Accounts',
              component: './Accounts/Accounts',
            }
          ],
        },
        {
          path: '/dashboard/administration',
          name: 'Administration',
          icon: 'table',
          routes: [
            {
              path: '/dashboard/administration/administration',
              name: 'Administration',
              component: './Administration/Administration',
            }
          ],
        },
        // {
        //   name: 'list.table-list',
        //   icon: 'table',
        //   path: '/dashboard/list',
        //   component: './ListTableList',
        // },
        {
          component: './404',
        },
      ],
    },
    {
      component: './404',
    },
  ],
  // Theme for antd: https://ant.design/docs/react/customize-theme-cn
  theme: {
    // ...darkTheme,
    'primary-color': defaultSettings.primaryColor,
  },
  // @ts-ignore
  title: false,
  ignoreMomentLocale: true,
  proxy: proxy[REACT_APP_ENV || 'dev'],
  manifest: {
    basePath: '/',
  },
});
